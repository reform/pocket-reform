/*
 SPDX-License-Identifier: BSD-3-Clause
 Copyright (c) 2024 Chris Hofstaedtler
*/
#pragma once

#define USB_VID_PIDCODES     0x1209
#define USB_VID_RASPBERRYPI  0x2E8A
#define USB_PID_MNT_POCKET_REFORM_INPUT_10    0x6D06
#define USB_PID_MNT_POCKET_REFORM_SYSCTL_10   0x6D07
#define USB_PID_RASPBERRYPI_PICO_SDK_CDC      0x000A

#define USB_STR_MANUFACTURER_MNT                        "MNT"
#define USB_STR_PRODUCT_MNT_POCKET_REFORM_INPUT_10      "Pocket Reform Input 1.0"
#define USB_STR_PRODUCT_MNT_POCKET_REFORM_SYSCTL_10     "Pocket Reform System Controller 1.0"
